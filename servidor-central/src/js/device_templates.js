export const esp32_energia = (configs) => {
    return (`<div class="col-sm" class="esp-device">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">ESP32 ${configs.type}</h5>
                    <div class="device-info">
                        <strong class="device-status">Online</strong>
                        <i class="fa fa-signal"></i>
                        <a href="#" class="btn btn-danger remove-device">X</a>
                    </div>
                </div>
                <div class="card-body">
                    <h6 class="card-text esp-address">Endereço MAC: ${configs.esp_id}</h6>
                    <h6 class="card-text input-device">Dispositivo de entrada: ${configs.input_device}</h6>
                    <h6 class="card-text output-device">Dispositivo de saída: ${configs.output_device}</h6>
                    <h6 class"card-text home-place">Local de atuação: ${configs.home_place}</h6>
                    <h6 class="card-text activate-alarm">Aciona alarme: ${configs.activate_alarm}</h6>
                    <h6 class="card-text temperature">Temperatura:</h6>
                    <h6 class="card-text humidity">Umidade:</h6>
                    <h6 class="card-text device-state">Estado do dispositivo:</h6>
                    <h6 class="card-text ouput-is-dim">Saída dimerizável: ${configs.output_is_dim}</h6>
                    <h6 class="card-text intensity">Intensidade: ${configs.intensity}</h6>
                    <a href="#" class="btn btn-primary activate-device">Acionar dispositivo</a>
                    <button class="btn btn-success config-device" data-mac-address="${configs.esp_id}"
                    data-toggle="modal" data-target=".bd-example-modal-sm">Configurar dispositivo</button>
                </div>
            </div>
        </div>`);
}

export const esp32_bateria = (configs) => {
    return (`<div class="col-sm" class="esp-device">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">ESP32 ${configs.type}</h5>
                    <div class="device-info">
                        <strong class="device-status">Online</strong>
                        <i class="fa fa-signal"></i>
                        <a href="#" class="btn btn-danger remove-device">X</a>
                    </div>
                </div>
                <div class="card-body">
                    <h6 class="card-text esp-address">Endereço MAC: ${configs.esp_id}</h6>
                    <h6 class="card-text input-device">Dispositivo de entrada: ${configs.input_device}</h6>
                    <h6 class="card-text output-device">Dispositivo de saída: ${configs.output_device}</h6>
                    <h6 class"card-text home-place">Local de atuação: ${configs.home_place}</h6>
                    <h6 class="card-text activate-alarm">Aciona alarme: ${configs.activate_alarm}</h6>
                    <h6 class="card-text device-state">Estado do dispositivo:</h6>
                    <h6 class="card-text ouput-is-dim">Saída dimerizável: ${configs.output_is_dim}</h6>
                    <h6 class="card-text intensity">Intensidade: ${configs.intensity}</h6>
                    <a href="#" class="btn btn-primary activate-device">Acionar dispositivo</a>
                    <button class="btn btn-success config-device" data-mac-address="${configs.esp_id}" 
                    data-toggle="modal" data-target=".bd-example-modal-sm">Configurar dispositivo</button>
                </div>
            </div>
        </div>`);
}