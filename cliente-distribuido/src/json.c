#include "cJSON.h"
#include "nvs_flash.h"

#include "json.h"
#include "utils.h"

#define ESP_BATERIA CONFIG_ESP_BATERIA
#define ESP_ENERGIA CONFIG_ESP_ENERGIA


void json_add_mac_address_to_object(cJSON * object) {
    cJSON_AddStringToObject(object, "esp_id", get_esp_mac_address_as_str());
}

char * json_set_float_message(float value) {
    cJSON *message = cJSON_CreateObject();
    json_add_mac_address_to_object(message);
    cJSON_AddNumberToObject(message, "value", value);

    return cJSON_Print(message);
}

char * json_set_state_message(char * type, int value) {
    cJSON *message = cJSON_CreateObject();
    json_add_mac_address_to_object(message);
    cJSON_AddStringToObject(message, "type", type);
    cJSON_AddNumberToObject(message, "value", value);

    return cJSON_Print(message);
}

char * json_set_connection_message() {
    cJSON *message = cJSON_CreateObject();
    json_add_mac_address_to_object(message);
    cJSON_AddStringToObject(message, "type", get_esp_mode());

    return cJSON_Print(message);
}

char * json_get_object_string(char * message, char * key) {
    cJSON *json_object = cJSON_Parse(message);
    cJSON *str = cJSON_GetObjectItem(json_object, key);

    return str->valuestring;
}

double json_get_object_value(char * message) {
    cJSON *json_object = cJSON_Parse(message);
    cJSON *value = cJSON_GetObjectItem(json_object, "value");

    return value->valuedouble;
}
