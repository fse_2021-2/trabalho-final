#ifndef GPIO_CONTROL_H
#define GPIO_CONTROL_H

/** Configura o botão present na ESP */
void gpio_control_config_button();

/** Configura o led para atuar no formato padrão (acionado/desligado) */
void gpio_control_config_led();

/** Configura o led para atuar no modo PWM */
void gpio_control_config_led_pwm();

/** Controla o acionamento do LED */
void gpio_control_set_led_level(int level);

/** Task que lida com o acionamento do botão */
void handleButtonInterruption(void *params);

#endif