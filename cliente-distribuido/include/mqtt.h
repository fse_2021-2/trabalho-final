#ifndef MQTT_H
#define MQTT_H

#include "mqtt_client.h"

/** Inicializa o MQTT */
void mqtt_start();

/** Lida com o evento de conexão (CONNECTED) */
void mqtt_handle_connection_event();

/** Lida com o recebimento de mensagens do Servidor Central */
void mqtt_handle_message_receive(esp_mqtt_event_handle_t);

/** Envia mensagem de conexão */
void mqtt_send_connection_message();

/** Envia mensagem no tópico fse2021/180033034/<comodo>/... */
void mqtt_send_message(char* topic_suffix, char* message);

/** Inscreve-se no tópico (fse2021/180033034/<comodo>/estado) 
 * para receber mudança de estado do LED */
void mqtt_led_subscribe();

#endif