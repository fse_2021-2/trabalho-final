#include "esp_event.h"
#include "esp_sleep.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/gpio.h"
#include "driver/rtc_io.h"

#include "mqtt.h"
#include "json.h"
#include "gpio_control.h"

#define LED 2   // GPIO LED (INPUT)
#define BOTAO 0 // GPIO BOTAO (OUTPUT)

xQueueHandle interruptionQueue;
extern EventGroupHandle_t eventGroupMQTTConnection;

static void IRAM_ATTR gpio_isr_handler(void *args) {
    int pin = (int) args;
    xQueueSendFromISR(interruptionQueue, &pin, NULL);
}

void gpio_control_config_button() {
    gpio_pad_select_gpio(BOTAO);
    gpio_set_direction(BOTAO, GPIO_MODE_INPUT);
    gpio_pulldown_en(BOTAO);
    gpio_pullup_dis(BOTAO);

    gpio_set_intr_type(BOTAO, GPIO_INTR_POSEDGE);
    gpio_install_isr_service(0);
    gpio_isr_handler_add(BOTAO, gpio_isr_handler, (void *) BOTAO);

    // Low Power
    // else { 
    //     gpio_wakeup_enable(BOTAO, GPIO_INTR_LOW_LEVEL);
    //     esp_sleep_enable_gpio_wakeup();
    // }

    interruptionQueue = xQueueCreate(5, sizeof(int));
}

void gpio_control_config_led() {
    gpio_pad_select_gpio(LED);
    gpio_set_direction(LED, GPIO_MODE_OUTPUT);
}

void gpio_control_config_led_pwm() {
    // Configuração do Timer
  ledc_timer_config_t timer_config = {
    .speed_mode = LEDC_LOW_SPEED_MODE,
    .duty_resolution = LEDC_TIMER_8_BIT,
    .timer_num = LEDC_TIMER_0,
    .freq_hz = 1000,
    .clk_cfg = LEDC_AUTO_CLK
  };
  ledc_timer_config(&timer_config);

  // Configuração do Canal
  ledc_channel_config_t channel_config = {
    .gpio_num = LED,
    .speed_mode = LEDC_LOW_SPEED_MODE,
    .channel = LEDC_CHANNEL_0,
    .timer_sel = LEDC_TIMER_0,
    .duty = 0,
    .hpoint = 0
  };

  ledc_channel_config(&channel_config);
}

void gpio_control_set_led_level(int level) {
    if (level == 0 || level == 100) {
        // Ativa o led no modo padrão
        gpio_control_config_led();
        gpio_set_level(LED, level);
    } else {
        // Ativa o led no modo PWM (Saída dimerizável)
        gpio_control_config_led_pwm();
        ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, level * 255 / 100);
        ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);
    }
}

void handleButtonInterruption(void *params) {
    int pin;
    int estado = 0;

    gpio_control_config_button(); // Configura interrupção do botão da ESP

    while (true) {
        // Aguarda a conexão com o Servidor Central
        xEventGroupWaitBits(eventGroupMQTTConnection, BIT0, false, true, portMAX_DELAY);

        if(xQueueReceive(interruptionQueue, &pin, portMAX_DELAY)) {
            if (gpio_get_level(pin) == 1) {
                gpio_isr_handler_remove(pin);
                
                while(gpio_get_level(pin) == 1) {
                    vTaskDelay(50 / portTICK_PERIOD_MS);
                }

                estado = !estado;
                mqtt_send_message("estado", json_set_state_message("input", estado));

                // Habilitar novamente a interrupção
                vTaskDelay(50 / portTICK_PERIOD_MS);
                gpio_isr_handler_add(pin, gpio_isr_handler, (void *) pin);
            }
        }

        // Low Power
        // else {
        //     if (rtc_gpio_get_level(BOTAO) == 0) {
        //         do {
        //             vTaskDelay(pdMS_TO_TICKS(10));
        //         } while (rtc_gpio_get_level(BOTAO) == 0);

        //         estado = !estado;
        //         mqtt_send_message("estado", json_set_int_message(estado));
        //     }
            
        //     vTaskDelay(1000 / portTICK_PERIOD_MS);
        //     esp_light_sleep_start();
        // }
    }
}
