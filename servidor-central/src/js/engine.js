import { esp32_energia, esp32_bateria } from "./device_templates";

const render_ESP_device = (add_new = false) => {
    const container_devices = document.getElementById('connected-devices');

    const esps = JSON.parse(localStorage.getItem('esp-devices'));

    if (!add_new) {
        for (let i = 0; i < esps.devices.length; i++) {
            let device = esps.devices[i];

            if (Object.keys(device).includes('type')) {
                if (device.type.toLowerCase() == 'energia')
                    container_devices.innerHTML += esp32_energia(device);
                else if (device.type.toLowerCase() == 'bateria')
                    container_devices.innerHTML += esp32_bateria(device);
            }
        }
    } else {
        let device = esps.devices[esps.devices.length - 1];

        if (Object.keys(device).includes('type')) {
            if (device.type.toLowerCase() == 'energia')
                container_devices.innerHTML += esp32_energia(device.esp_id, device.type);
            else if (device.type.toLowerCase() == 'bateria')
                container_devices.innerHTML += esp32_bateria(device.esp_id, device.type);
        }
    }
}

export const render_config_modal = () => {
    const container = document.getElementsByClassName('modal-content')[0];
}

const humidity_handler = () => {

}

const temperature_handler = () => {

}

const config_device_handler = (event) => {
    event.preventDefault();

    console.log('Configura dispositivo');
    let mac_address = event.target.dataset.macAddress;
    localStorage.setItem('device-to-config', `${mac_address}`);

    console.log(mac_address);
}

const activate_device_handler = (event) => {
    event.preventDefault();

    console.log('Ativa dispositivo');
}

const remove_device_handler = () => {
    console.log('Remove dispositivo');
}

const alarm_handler = (event) => {
    event.preventDefault();

    const alarm_element = document.querySelector('a#alarm-status');
    if (alarm_element.innerHTML == 'Desligar') {
        alarm_element.innerHTML = 'Ligar';
    } else {
        alarm_element.innerHTML = 'Desligar';
    }
}

const send_config_handler = (event) => {
    event.preventDefault();

    let input_device = document.getElementById('input-device').value;
    let output_device = document.getElementById('output-device').value;
    let home_place = document.getElementById('home-place').value;
    let activate_alarm = document.getElementById('activate-alarm').value;
    let output_is_dim = document.getElementById('output-is-dim').value;
    let intensity = document.getElementById('intensity').value;

    let configs = {
        input_device,
        output_device,
        home_place,
        activate_alarm,
        output_is_dim,
        intensity
    }

    localStorage.setItem('last_configured', JSON.stringify(configs));

    let configured_device = localStorage.getItem('device-to-config');
    console.log(configured_device);
    let esps = JSON.parse(localStorage.getItem('esp-devices'));
    
    for (let i = 0; i < esps.devices.length; i++){
        if (esps.devices[i].esp_id == configured_device) {
            esps.devices[i].input_device = input_device;
            esps.devices[i].output_device = output_device;
            esps.devices[i].home_place = home_place;
            esps.devices[i].activate_alarm = activate_alarm;
            esps.devices[i].output_is_dim = output_is_dim;
            esps.devices[i].intensity = intensity;
        }
    }

    localStorage.setItem('esp-devices', JSON.stringify(esps));

    document.location.reload();
}

export { render_ESP_device, alarm_handler, activate_device_handler, config_device_handler, remove_device_handler,
        send_config_handler };