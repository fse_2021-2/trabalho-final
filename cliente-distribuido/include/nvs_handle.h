#ifndef NVS_HANDLE_H
#define NVS_HANDLE_H

/** Lê local/cômodo salvo na memória NVS */
char * read_nvs_local();

/** Salva local/cômodo na memória NVS */
void set_nvs_local(char *);

#endif