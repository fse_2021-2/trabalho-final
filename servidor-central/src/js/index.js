import { alarm_handler, render_ESP_device, activate_device_handler, config_device_handler,
        remove_device_handler, render_config_modal, send_config_handler } from "./engine";
import * as mqtt from "mqtt";

const client = mqtt.connect('mqtt://test.mosquitto.org:8081');

const subscribe_to_topic = async (topic = '') => {
    client.on('connect', function () {
        client.subscribe(topic, function (err) {
            if (err) {
                alert(`Erro ao se inscrever no tópico: ${topic}`);
            }
            console.log('Se inscreveu!');
        })
    })
}

const publish_to_topic = (topic, message) => {
    client.publish(topic, message);
}

const wait_for_message = async => {
    client.on('message', function (topic, message) {
        message.toString();
        message = JSON.parse(message);
        message.topic = topic;
        let esp_registered = false;

        if (/fse2021\/180033034\/dispositivos\/\w+/.test(message.topic)) {
            let esps = JSON.parse(localStorage.getItem('esp-devices'));
            
            for (let i = 0; i < esps.devices.length; i++){
                if (message.esp_id == esps.devices[i].esp_id){
                    esp_registered = true;
                    break;
                }
            }

            if (!esp_registered){
                esps.devices.push(message);
                localStorage.setItem('esp-devices', JSON.stringify(esps));

                render_ESP_device(true);
            }
            
            const activate_device_btns = document.querySelectorAll('a.activate-device');
            const config_device_btns = document.querySelectorAll('a.config-device');
            const remove_device_btns = document.querySelectorAll('a.remove-device');

            for (let i = 0; i < activate_device_btns.length; i++) {
                activate_device_btns[i].addEventListener('click', activate_device_handler);
                config_device_btns[i].addEventListener('click', config_device_handler);
                remove_device_btns[i].addEventListener('click', remove_device_handler);
            }

        }
    })
}

const init = () => {
    const alarm_element = document.querySelector('a#alarm-status');
    const device_config_save = document.querySelector('button#send-config');

    alarm_element.addEventListener('click', alarm_handler);
    device_config_save.addEventListener('click', send_config_handler);

    render_ESP_device();

    subscribe_to_topic('fse2021/180033034/dispositivos/#');

    wait_for_message();

}

init();