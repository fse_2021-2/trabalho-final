#ifndef SENSOR_DHT11_H
#define SENSOR_DHT11_H

/** Inicializa o sensor DHT11 e cria as filas de temperatura e umidade */
void sensor_dht11_init();

/** Task de Leitura do sensor DHT11 a cada 2s */
void handleDHT11SensorRead(void* params);

/** Task para calcular a média da temperatura e umidade e 
 * enviar para o servidor central a cada 10s */
void sendAvgTemperatureAndHumidityToServer(void* params);

#endif