#include "dht11.h"
#include "esp_log.h"
#include "esp_event.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "mqtt.h"
#include "json.h"
#include "utils.h"
#include "sensor_dht11.h"

#define TAG "DHT11"

extern EventGroupHandle_t eventGroupMQTTConnection;

xQueueHandle temperatureQueue;
xQueueHandle humidityQueue;

void sensor_dht11_init() {
    DHT11_init(GPIO_NUM_4);

    temperatureQueue = xQueueCreate(5, sizeof(float));
    humidityQueue = xQueueCreate(5, sizeof(float));
}

void handleDHT11SensorRead(void* params) {
    int temperature, humidity;

    sensor_dht11_init();

    while (true) {
        // Aguada conexão com o Servidor Central
        xEventGroupWaitBits(eventGroupMQTTConnection, BIT0, false, true, portMAX_DELAY);

        if (DHT11_read().status == DHT11_OK) {
            // Leitura da temperatura e umidade via DHT11
            temperature = DHT11_read().temperature;
            humidity = DHT11_read().humidity;
        } else {
            ESP_LOGI(TAG, "Erro na leitura da Temperatura via DHT11");
            temperature = 0; humidity = 0;
        }

        ESP_LOGI(TAG, "Temperatura Lida: %d", temperature);
        ESP_LOGI(TAG, "Umidade Lida: %d", humidity);

        xQueueSend(temperatureQueue, &temperature, 1000 / portTICK_PERIOD_MS);
        xQueueSend(humidityQueue, &humidity, 1000 / portTICK_PERIOD_MS);

        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}

void sendAvgTemperatureAndHumidityToServer(void* params) {
    int count = 0;
    int temperature = 0, sum_temperature = 0;
    int humidity = 0, sum_humidity = 0;
    float avg_temperature = 0.0, avg_humidity = 0.0;

    while (true) {
        // Aguada conexão com o Servidor Central
        xEventGroupWaitBits(eventGroupMQTTConnection, BIT0, false, true, portMAX_DELAY);

        if(xQueueReceive(temperatureQueue, &temperature, 2000 / portTICK_PERIOD_MS)) {
            xQueueReceive(humidityQueue, &humidity, 2000 / portTICK_PERIOD_MS);

            sum_temperature += temperature;
            sum_humidity += humidity;

            count++;
            if (count == 5) {
                avg_temperature = sum_temperature / 5.0;
                avg_humidity = sum_humidity / 5.0;

                mqtt_send_message("temperatura", json_set_float_message(avg_temperature));
                mqtt_send_message("umidade", json_set_float_message(avg_humidity));

                count = 0; sum_temperature = 0; sum_humidity = 0;
            }
        }
    }
}
