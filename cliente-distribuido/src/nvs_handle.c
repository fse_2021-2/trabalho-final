#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"

#include "nvs_handle.h"

#define TAG "NVS"

char * read_nvs_local() {
    ESP_ERROR_CHECK(nvs_flash_init());

    nvs_handle particao_padrao_handle;
    esp_err_t res_nvs = nvs_open("armazenamento", NVS_READONLY, &particao_padrao_handle);

    if(res_nvs == ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGE("NVS", "Namespace: armazenamento, não encontrado");

        return "NOT FOUND";
    }
    else {
        size_t size;
        nvs_get_str(particao_padrao_handle, "local", NULL, &size);
        char * local = malloc(size);
        esp_err_t res = nvs_get_str(particao_padrao_handle, "local", local, &size);

        nvs_close(particao_padrao_handle);

        switch (res) {
            case ESP_OK:
                return local;
            case ESP_ERR_NOT_FOUND:
                ESP_LOGE(TAG, "Local não Encontrado");
                return "NOT FOUND";
            default:
                ESP_LOGE(TAG, "Erro ao acessar o NVS (%s)", esp_err_to_name(res));
                return "NOT FOUND";
                break;
        }
    }
}

void set_nvs_local(char *local) {
    ESP_ERROR_CHECK(nvs_flash_init());

    nvs_handle particao_padrao_handle;
    esp_err_t res_nvs = nvs_open("armazenamento", NVS_READWRITE, &particao_padrao_handle);
    
    if(res_nvs == ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGE("NVS", "Namespace: armazenamento, não encontrado");
    }

    esp_err_t res = nvs_set_str(particao_padrao_handle, "local", local);
    if(res != ESP_OK) {
        ESP_LOGE("NVS", "Não foi possível escrever no NVS (%s)", esp_err_to_name(res));
    }

    ESP_LOGI(TAG, "Local Armazenado: %s", local);

    nvs_commit(particao_padrao_handle);
    nvs_close(particao_padrao_handle);
}
