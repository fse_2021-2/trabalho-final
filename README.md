# FSE: Trabalho Final - 2021.2.

**Membro 1:** Hérick Ferreira de Souza Portugues - 18/0033034

**Membro 2:** Maicon Lucas Mares de Souza - 18/0023411

**Descrição:** Este trabalho refere-se ao definido em: https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-final-2021-2

## Demonstração

O funcionamento do sistema pode ser conferido no vídeo: https://youtu.be/olJAmxXuzag

## Comunicação

As comunicações MQTT entre a ESP e o Servidor Central foram definidas da seguinte maneira:

```
// Mensagens enviadas pela ESP para o servidor central (Umidade, Temperatura)
// tópico: fse2021/<matricula>/<comodo>/temperatura
// tópico: fse2021/<matricula>/<comodo>/umidade
{
  'esp_id': mac_address,
  'value': float,
}

// Mensagens enviadas pela ESP para o servidor central (Botão)
// tópico: fse2021/<matricula>/<comodo>/estado
{
  'esp_id': mac_address,
  'type': 'input',
  'value': int (0-1),
}

// Mensagens enviadas pelo Servidor Central para a ESP (LED)
// tópico: fse2021/<matricula>/<comodo>/estado
{
  'esp_id': mac_address,
  'type': 'output',
  'value': int (0-100),
}

// Mensagem de conexão (ESP -> Servidor Central)
// tópico: fse2021/<matricula>/dispositivos/<id-dispositivo>
{
  'esp_id': mac_address,
  'type': 'BATERIA / ENERGIA',
}

// Mensagem de retorno ao conectar (Servidor Central -> ESP)
// tópico: fse2021/<matricula>/local
{
  'esp_id': mac_address,
  'local': 'quarto, etc',
}
```

## Como Rodar

### Servidor Central:

- npm run install
- npm run dev
- Abra o browser no endereço localhost:8080

### Cliente Distríbuido:

A programação da ESP foi realizada utilizando a extensão PlatformIO do VSCode. Antes de rodar é necessário entrar no menuconfig (Opção "Run Menuconfig" na aba do PlatformIO) e configurar o WiFi no menu "Configuração do WIFI". Também é possível é alterar o modo de execução da ESP (BATERIA / ENERGIA) através do menu "Configuração da ESP". Para rodar, basta acessar a aba do PlatformIO e rodar "Build", seguido de "Upload and Monitor".
