#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "esp_system.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "mqtt_client.h"

#include "mqtt.h"
#include "json.h"
#include "utils.h"
#include "nvs_handle.h"
#include "gpio_control.h"

#define TAG "MQTT"

extern EventGroupHandle_t eventGroupMQTTConnection;
extern xSemaphoreHandle mqttConnectionSemaphore;

esp_mqtt_client_handle_t client;

static esp_err_t mqtt_event_handler_callback(esp_mqtt_event_handle_t event) {
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            mqtt_handle_connection_event();
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            mqtt_handle_message_receive(event);
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_callback(event_data);
}

void mqtt_start() {
    esp_mqtt_client_config_t mqtt_config = {
        .uri = "mqtt://test.mosquitto.org:1883",
    };
    client = esp_mqtt_client_init(&mqtt_config);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);
}

void mqtt_handle_connection_event() {
    if (strcmp(read_nvs_local(), "NOT FOUND") == 0) {
        mqtt_send_connection_message(); // ESP ainda não cadastrada. Envia mensagem de conexão
        esp_mqtt_client_subscribe(client, "fse2021/180033034/local", 2); // Subscribe para receber a mensagem de retorno
    } else {
        // ESP já cadastrada.
        ESP_LOGI(TAG, "ESP já cadastrada -> Local: %s", read_nvs_local());
        xEventGroupSetBits(eventGroupMQTTConnection, BIT0);

        if (is_esp_energia()) {
            mqtt_led_subscribe();
        }
    }
}

void mqtt_handle_message_receive(esp_mqtt_event_handle_t event) {
    char topic[100];

    // Verifica o mac_address para conferir se a mensagem
    // enviada pelo servidor central é para esta ESP
    if (strcmp(json_get_object_string(event->data, "esp_id"), get_esp_mac_address_as_str()) == 0) {
        sprintf(topic, "%.*s", event->topic_len, event->topic);

        
        if (strcmp(topic, "fse2021/180033034/local") == 0) { // Retorno da mensagem de conexão
            // Salva o local na memória NVS
            set_nvs_local(json_get_object_string(event->data, "local"));

            // Libera as tasks do dht11 + botão
            xEventGroupSetBits(eventGroupMQTTConnection, BIT0);

            if (is_esp_energia()) {
                mqtt_led_subscribe();
            }
        } else {
            if (strcmp(json_get_object_string(event->data, "type"), "output") == 0) {
                // Mensagem do Servidor Central para a ESP (LED)
                int led_level = (int) json_get_object_value(event->data);
                gpio_control_set_led_level(led_level);
            }
        }
    }
}

void mqtt_send_connection_message() {
    char connection_topic[100];
    strcpy(connection_topic, "fse2021/180033034/dispositivos/");
    strcat(connection_topic, get_esp_mac_address_as_str());

    esp_mqtt_client_publish(client, connection_topic, json_set_connection_message(), 0, 2, 0);
}

void mqtt_send_message(char* topic_suffix, char* message) {
    char topic[100];
    sprintf(topic, "fse2021/180033034/%s/%s", read_nvs_local(), topic_suffix);

    int msg_id = esp_mqtt_client_publish(client, topic, message, 0, 1, 0);
}

void mqtt_led_subscribe() {
    char topic[100];
    sprintf(topic, "fse2021/180033034/%s/estado", read_nvs_local());
    esp_mqtt_client_subscribe(client, topic, 0);
}
