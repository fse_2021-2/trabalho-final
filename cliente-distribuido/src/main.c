#include <stdio.h>
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "freertos/semphr.h"
#include "freertos/event_groups.h"

#include "wifi.h"
#include "mqtt.h"
#include "utils.h"
#include "sensor_dht11.h"
#include "gpio_control.h"

xSemaphoreHandle wifiConnectionSemaphore;
EventGroupHandle_t eventGroupMQTTConnection;

void handleWifiConnection(void* params) {
    while(true) {
        if(xSemaphoreTake(wifiConnectionSemaphore, portMAX_DELAY)) {
            mqtt_start();
        }
    }
}

void app_main() {
    // Inicializa o NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    eventGroupMQTTConnection = xEventGroupCreate();
    wifiConnectionSemaphore = xSemaphoreCreateBinary();

    wifi_start(); // Conecta ao WIFI (Necessário configuração no Menuconfig)

    xTaskCreate(&handleWifiConnection, "Conexão WIFI + MQTT", 2048, NULL, 1, NULL);
    xTaskCreate(&handleButtonInterruption, "Trata Acionamento do Botão", 4096, NULL, 1, NULL);
    
    if (is_esp_energia()) {
        xTaskCreate(&handleDHT11SensorRead, "Lê Temperatura e Humidade", 2048, NULL, 1, NULL);
        xTaskCreate(&sendAvgTemperatureAndHumidityToServer, "Envia Média da Temperatura", 2048, NULL, 1, NULL);
    }
}