#include "esp_netif.h"
#include "string.h"

#include "utils.h"

#if defined(CONFIG_ESP_ENERGIA)
#define ESP_MODE "ENERGIA"
#else
#define ESP_MODE "BATERIA"
#endif

uint8_t mac_address[6];
char esp_id[50];

char * get_esp_mac_address_as_str() {
    esp_base_mac_addr_get(mac_address);
    sprintf(esp_id, MACSTR, MAC2STR(mac_address));
    return esp_id;
}

char * get_esp_mode() {
    return ESP_MODE;
}

bool is_esp_energia() {
    return strcmp(ESP_MODE, "ENERGIA") == 0;
}