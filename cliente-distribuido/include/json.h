#ifndef JSON_H
#define JSON_H

/** Cria mensagem de temperatura/umidade */
char * json_set_float_message(float);

/** Cria mensagem com o estado do botão */
char * json_set_state_message(char *, int);

/** Cria mensagem de conexão */
char * json_set_connection_message();

/** Retorna o valor contido na chave "value" da mensagem */
double json_get_object_value(char * message);

/** Retorna uma string presente no objeto */
char * json_get_object_string(char * message, char * key);

#endif