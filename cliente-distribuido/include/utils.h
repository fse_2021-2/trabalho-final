#ifndef UTILS_H
#define UTILS_H

/** Retorna o mac_address da ESP como char * */
char * get_esp_mac_address_as_str();

/** Retorna o modo da ESP (BATERIA ou ENERGIA)*/
char * get_esp_mode();

/** Retorna verdadeiro se a ESP estiver no modo ENERGIA*/
bool is_esp_energia();

#endif